using AngleSharp;
using AngleSharp.Dom;
using System.Configuration;
using Configuration = AngleSharp.Configuration;
namespace txtdown
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
        }
        public class Belle
        {
            public string Title { get; set; }
            public string Url { get; set; }
            public string Text { get; set; }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tbWZ.Text = "https://www.xs123.co/xs/3/3869/";
            tbML.Text = "#list dd a";
            tbNR.Text = "#content";

            button3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbWZ.Text = "";
            tbML.Text = "";
            tbNR.Text = "";
            lbMC.Text = "";
            tbMC.Text = "";
            richTextBox1.Text = "";
            listView1.Items.Clear();//清空菜单
            listView1.Columns.Clear();
        }

        /// <summary>
        /// 一级菜单
        /// </summary>
        private void LoadList(string Url, string sel, string se2)
        {
            if (string.IsNullOrEmpty(tbWZ.Text) || string.IsNullOrEmpty(tbML.Text) || string.IsNullOrEmpty(tbMC.Text))
            {
                MessageBox.Show("请填写书名、网址和目录规则~");
                return;
            }
            // 设置配置以支持文档加载
            var config = Configuration.Default.WithDefaultLoader();
            // 获取目录
            var document = BrowsingContext.New(config).OpenAsync(Url);
            // 根据class获取html元素
            IHtmlCollection<IElement>? cells = document.Result.QuerySelectorAll(sel);
            //获取目录
            var list = cells.Select(m => new { m.TextContent, Href = ((AngleSharp.Html.Dom.HtmlUrlBaseElement)m).Href });

            lbMC.Text = $"《{tbMC.Text}》";
            ImageList image = new ImageList();
            image.ImageSize = new Size(1, 35);//设置每次点击view时以图片的形式
            ColumnHeader ch = new ColumnHeader();
            ch.Text = $"《{tbMC.Text}》";
            ch.Width = splitContainer1.Panel1.Width;
            ch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            listView1.Columns.Add(ch);//设置listview的列名，没啥用处
            listView1.SmallImageList = image;//设置每个view的显示形式

            listView1.Items.Clear();//清空菜单
            foreach (var item in list)
            {
                this.listView1.Items.Add(new ListViewItem { Text = item.TextContent, Name = item.Href });
            }
            listView1.Items[listView1.Items.Count - 1].EnsureVisible();//滚动到控件光标处 
        }
        private async void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            button3.Enabled = false;
            var t = Task.Run(() =>
            {
                LoadList(tbWZ.Text, tbML.Text, tbNR.Text);
            });
            await t;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lbMC.Text) || lbMC.Text == "书名")
            {
                MessageBox.Show("先点击确认，获取目录~");
                return;
            }
            button3.Enabled = false;
            richTextBox1.Text = "";
            List<Belle> list = new List<Belle>();

            foreach (ListViewItem item in listView1.Items)
            {
                list.Add(new Belle
                {
                    Title = item.Text,
                    Url = item.Name
                });
            }
            await Parallel.ForEachAsync(list, new ParallelOptions()
            {
                MaxDegreeOfParallelism = 10
            }, async (obj, _) =>
            {
                richTextBox1.AppendText($"正在下载：{obj.Title}...\r\n");
                // 设置配置以支持文档加载
                var config = Configuration.Default.WithDefaultLoader();
                // 获取目录
                var document = await BrowsingContext.New(config).OpenAsync(obj.Url);

                IElement? title = document.QuerySelector(tbNR.Text);

                obj.Text = title.InnerHtml.Replace("<br>", "\n");
                richTextBox1.ScrollToCaret();//滚动到控件光标处 
            });

            richTextBox1.AppendText($"下载已完成，请转到桌面查看~~~");

            StreamWriter file = new($@"{Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)}\{lbMC.Text}.txt", false);
            foreach (var line in list)
            {
                file.WriteLine(line.Title);// 直接追加文件末尾，换行
                file.WriteLine(line.Text);// 直接追加文件末尾，换行
            }
            file.Dispose();
            MessageBox.Show("下载成功，请转到桌面查看~");
            button3.Enabled = true;

        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrEmpty(tbNR.Text))
            {
                MessageBox.Show("请填写内容规则~");
                return;
            }
            Point tmpPoint = listView1.PointToClient(Cursor.Position);
            ListViewItem.ListViewSubItem subitem = listView1.HitTest(tmpPoint).SubItem;

            // 设置配置以支持文档加载
            var config = Configuration.Default.WithDefaultLoader();
            // 获取目录
            var document = BrowsingContext.New(config).OpenAsync(subitem.Name);
            // 根据class获取html元素
            IElement? title = document.Result.QuerySelector(tbNR.Text);

            richTextBox1.Text = title.InnerHtml.Replace("<br>", "\n");
        }
    }
}